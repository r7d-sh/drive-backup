#!/bin/bash
# Author: Daniel Podolski 
# Date: 1.11.2019
# Comment: Bash script for creating linux backups 

set -e
parseJsonConfig () {
	echo "$(tput setaf 125)Parse config file ""$config""...$(tput sgr0)"
	remotes=$(jq '. | length' "$config")
	echo "$(tput setaf 42)done.$(tput sgr0)"	
	echo
}

loopRemotes () {
	for (( i=0; i< "$remotes"; i++ ))
	do  
		remoteAddress=$(jq -r ".[$i].address" "$config")
		remoteUser=$(jq -r ".[$i].user" "$config")
		pemFile=$(jq -r ".[$i].pemFile" "$config")
		localBackupDir=$(jq -r ".[$i].localBackupDir" "$config")
		localImage=$(jq -r ".[$i].localImage" "$config")
		drive=$(jq -r ".[$i].drive" "$config")
		timeStamp=$(date +%Y%m%d%H%M%S)
		backupDir="$localBackupDir/$timeStamp"

		echo "$(tput setaf 125)Create backup directory at $backupDir...$(tput sgr0)"
		if [ ! -d "$backupDir"  ]; then
			mkdir "$backupDir"
		else
			echo "$(tput setaf 125)$backupDir already exists, cleaning up...$(tput sgr0)"
			rm -rf "$backupDir"/* 
		fi
		echo "$(tput setaf 42)done.$(tput sgr0)"	
		echo

		backupImageDir="$backupDir/$localImage"

		echo "$(tput setaf 125)Create image file at $backupImageDir...$(tput sgr0)"
	
		if [ ! -f "$backupImageDir"  ]; then
			touch "$backupImageDir"
		else
			echo "$(tput setaf 125)$backupImageDir already exists, exiting...$(tput sgr0)"
			exit 1 
		fi

		echo "$(tput setaf 42)done.$(tput sgr0)"	
		echo

		echo "$(tput setaf 125)Start drive backups...$(tput sgr0)"
		echo "$(tput setaf 125)Remote machine : $remoteAddress...$(tput sgr0)"
		echo "$(tput setaf 125)Backing up drive : $drive...$(tput sgr0)"
		echo "$(tput setaf 125)Destination : $backupImageDir...$(tput sgr0)"
		ssh -i "$pemFile" "$remoteUser"@"$remoteAddress" "dd if=$drive" | dd of="$backupImageDir" status=progress
		echo "$(tput setaf 42)done.$(tput sgr0)"	
		echo

	done
}

main () {
    currDirName=$(dirname "$0")
	
	config="$currDirName/backup_config.json"

    if [ ! -f "$config" ]; then
	    echo "$config not found!"
	    exit 1
	fi

	parseJsonConfig

	loopRemotes
}

main

