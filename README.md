# Welcome to drive-backup

Create automatic backups of complete drives.

### About

If there is a need to store Linux backups on a form of local storage.
This script is performing automatic drive backup.
Executed from storage machine, there are no additional files needed on the remote Linux machine

##### With drive-backup you can:

* Restore your drive, from local backup after your instance is broken.

### Files

Each script run creates a new directory structure 

Example: 

* 20191111150530 
    * sda.img
    * sdd.img


### Requirements

* Jq on storage machine.
* Configured ssh connection 

### Config
Edit the backup_config.json file according to your configuration.

    [{
        "address": "192.168.0.1",					// Linux machine address 
        "user": "root",								// Remote machine username 
        "pemFile": "/path/to/pem/file",             // Pem file location 
        "localBackupDir": "/path/to/local/backup",  // Local backup storage 
        "localImage": "sda.img",					// Local image file name 
        "drive": "/dev/sda"							// Remote drive to backup 
    }]

### Usage

Run the script on your backup machine
    
    # ./backup.sh 